<?php

/**
 * @file 
 * Defines the attribute_password class and related functions.
 */

/**
 * LDAP attribute_password Class
 *
 *   This class provides additional functions, variables, and consideration for
 * password attributes.
 */
class ldap_attribute_password extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
