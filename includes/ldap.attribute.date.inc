<?php

/**
 * @file 
 * Defines the attribute_date class and related functions.
 */

/**
 * LDAP attribute_date Class
 *
 *   This class provides additional functions, variables, and consideration for
 * date attributes.
 */
class ldap_attribute_date extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
