<?php

/**
 * @file 
 * Defines the attribute_dn class and related functions.
 */

/**
 * LDAP attribute_dn Class
 *
 *   This class provides additional functions, variables, and consideration for
 * distinguishedname attributes.
 */
class ldap_attribute_dn extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
