<?php

/**
 * @file 
 * Defines the attribute_multiline class and related functions.
 */

/**
 * LDAP attribute_multiline Class
 *
 *   This class provides additional functions, variables, and consideration for
 * multiline attributes.
 */
class ldap_attribute_multiline extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
