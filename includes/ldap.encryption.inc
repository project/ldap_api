<?php
// $Id $

/**
 * @file 
 * Provides functions for encryption/decryption.
 *
 * @see RFC4513
 */

/**
 * Return a random salt of a given length for crypt-style passwords
 *
 * @param int length
 *   The requested length.
 *
 * @return string
 *   A (fairly) random salt of the requested length.
 *
 */
function random_salt( $length ) {
  $possible = '0123456789'.
              'abcdefghijklmnopqrstuvwxyz'.
              'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
              './';
  $salt = "";

  mt_srand((double)microtime() * 1000000);
  while( strlen( $salt ) < $length ) {
    $salt .= substr( $possible, ( rand() % strlen( $possible ) ), 1 );
  }

  return $salt;
}

/**
 * Encrypt Password Method
 *
 * @param string clearTxt
 *   Plaintext password.
 *
 * @return string 
 *   Encrypted text, formatted for use as an LDAP password.
 *
 */
function encrypt_password( $clearTxt ) {
  global $_ldapdata_ldap;
    
  switch ($_ldapdata_ldap->getOption('enc_type')) {
    case 1: // MD5
      $cipherTxt = '{MD5}' . base64_encode( pack( 'H*' , md5( $clearTxt) ) );
      break;

    case 2: // Crypt
      $cipherTxt = '{CRYPT}' . crypt($clearTxt,substr($clearTxt,0,2));
      break;

    case 3: // Salted Crypt
      $cipherTxt = '{CRYPT}' . crypt($clearTxt,random_salt(2));
      break;

    case 4: // Extended DES
      $cipherTxt = '{CRYPT}' . crypt( $clearTxt, '_' . random_salt(8) );
      break;

    case 5: // MD5Crypt
      $cipherTxt = '{CRYPT}' . crypt( $clearTxt , '$1$' . random_salt(9) );
      break;

    case 6: // Blowfish
      $cipherTxt = '{CRYPT}' . crypt( $clearTxt , '$2a$12$' . random_salt(13) );
      break;

    case 7: // Salted MD5
      mt_srand( (double) microtime() * 1000000 );
      $salt = mhash_keygen_s2k( MHASH_MD5, $clearTxt, substr( pack( "h*", md5( mt_rand() ) ), 0, 8 ), 4 );
      $cipherTxt = "{SMD5}".base64_encode( mhash( MHASH_MD5, $clearTxt.$salt ).$salt );
      break;

    case 8: // SHA
      if ( function_exists('sha1') ) {
        $cipherTxt = '{SHA}' . base64_encode( pack( 'H*' , sha1( $clearTxt) ) );
      } elseif ( function_exists( 'mhash' ) ) {
        $cipherTxt = '{SHA}' . base64_encode( mhash( MHASH_SHA1, $clearTxt) );
      }
      break;

    case 9: // Salted SHA
      mt_srand( (double) microtime() * 1000000 );
      $salt = mhash_keygen_s2k( MHASH_SHA1, $clearTxt, substr( pack( "h*", md5( mt_rand() ) ), 0, 8 ), 4 );
      $cipherTxt = "{SSHA}".base64_encode( mhash( MHASH_SHA1, $clearTxt.$salt ).$salt );
      break;

    default: // Cleartext
      $cipherTxt = $clearTxt;
  }

  return $cipherTxt;
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
