<?php

/**
 * @file 
 * Defines the attribute_binary class and related functions.
 */

/**
 * LDAP attribute_binary Class
 *
 *   This class provides additional functions, variables, and consideration for
 * binary attributes.
 */
class ldap_attribute_binary extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
