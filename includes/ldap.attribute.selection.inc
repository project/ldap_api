<?php

/**
 * @file 
 * Defines the attribute_selection class and related functions.
 */

/**
 * LDAP attribute_selection Class
 *
 *   This class provides additional functions, variables, and consideration for
 * selection attributes.
 */
class ldap_attribute_selection extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
