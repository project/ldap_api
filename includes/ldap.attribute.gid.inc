<?php

/**
 * @file 
 * Defines the attribute_gid class and related functions.
 */

/**
 * LDAP attribute_gid Class
 *
 *   This class provides additional functions, variables, and consideration for
 * GID Attributes.
 */
class ldap_attribute_gid extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
