<?php

/**
 * @file 
 * Defines the attribute_jpeg class and related functions.
 */

/**
 * LDAP attribute_jpeg Class
 *
 *   This class provides additional functions, variables, and consideration for
 * jpeg attributes.
 */
class ldap_attribute_jpeg extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
