<?php

/**
 * @file 
 * Defines the attribute_object class and related functions.
 */

/**
 * LDAP attribute_object Class
 *
 *   This class provides additional functions, variables, and consideration for
 * objectclass attributes.
 */
class ldap_attribute_object extends ldap_attribute {
}

// vim:fenc=utf-8:ft=php:ai:si:ts=2:sw=2:et:
